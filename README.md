# Git to Debian source package notes

Some notes on how to convert an external Git repository to a Debian source package Git 

These are simple notes to learn how to convert some source code from an external Git repo (here github) into a clean Debian source git repo (here salsa).

The Debian Git repo is a 'blank' git, with 3 branches:
- the '**upstream**' holds the raw source code
- the '**master**' contains the debian files (control, rules, patches, etc)
- the '**pristine-tar**' purpose is to compare the current master with the initial source code
 
**WARNING**: The initial source code must **not** be touched. If you ever need some adaption, use patches. patches are managed with e.g. 'quilt'. 
You will work on the 'master' branch, not on the pristine-tar nor the upstream.

--------------------------------------------------------------------------------

## Create a new Debian source package from an upstream

### Step 1: get the source code from a tar.gz

We get a tar.gz from somewhere
```bash
wget https://github.com/biochem-fan/eiger2cbf/archive/master.tar.gz
```

The source code must be renamed as Debian-like name ```<package>_<version>.orig.tar.gz```
```bash
mv master.tar.gz eiger2cbf_20181214.orig.tar.gz
```

This archive must be present on the top level when creating our new Debian git repository.


### Step 2: create a new repo for the Debian build

You may create a new empty repository on [salsa.debian.org](https://salsa.debian.org/), 
then clone it and you're done for this step. This is our prefered way.

Alternatively, you can create it yourself manually. First create a directory to initiate our new repo.
```bash
mkdir eiger2cbf
cd eiger2cbf
```

and we initiate it:
```bash
git init
```

### Step 3: import the sources from the tar.gz

At this step we initiate our new Git repo by importing the source code and create branches:
- master   (debian)
- upstream (source code from tar.gz)
- pristine-tar (tar)

The original tar.gz must be present in the parent directory (from Step 1 above).
```bash
gbp import-orig --pristine-tar ../eiger2cbf_20181214.orig.tar.gz
```

which creates the 'upstream' and 'pristine-tar' branches.

The source code can then be removed
```bash
rm ../eiger2cbf_20181214.orig.tar.gz
```

**NOTE**: remember to push the pristine-tar and upstream branches altogether with the master at the end (see Step 5).

### Step 4: add a debian folder

Often the initial source code needs some adaption in order to properly build on a Debian system. 
As the source code must not be touched, all changes should be recorded in the 'debian' directory.
In addition, this folder also holds additional informaion related to the source code, e.g. licenses, dependencies, etc.

For the purpose of this example, we have set-up an initial debian folder that already contains information and patches. 
This is just a starting point, and you will most probably need few patches to adapt the 'upstream' source code to compile in a Debian environment.

You should make sure to work on the 'master' branch to hold the debian stuff:
```bash
git checkout master
```
then the idea is to create a 'debian' directory here. It can be done with an empty template with command:
```
dh_make
```

But, for the purpose of this example, we have prepared an archive that contains the proper files for this source code.
Get it at:
- https://salsa.debian.org/farhi-guest/git-to-debian-source-package-notes/-/blob/master/debian.zip

You may also start from a prepared template debian folder such as the one from Debian Med Team:
```bash
git clone https://salsa.debian.org/med-team/community/package_template.git /tmp/package_template
mv /tmp/package_template/debian . ; rm -rf /tmp/package_template
```

Then record your changes
```bash
git add debian/
git commit -m "add Debian folder"
```

### Step 5: adapt the source code


**NOTE**: ```gbp pq``` can do most of the following tasks. 
See https://wiki.debian.org/Python/GitPackaging#Patching

As said before, all changes on the source code should be recorded as patches. 
This can be done with `quilt`.
We use ```dquilt``` as defined from https://www.debian.org/doc/manuals/maint-guide/modify.en.html

To use quilt, simply indicate that you wish to create a new entry:
```bash
dquilt new name-of-my-patch.patch
```
then indicate which file is touched
```bash
dquilt add Makefile
```
Edit the code and actually tell 'quilt' to compute and register the patch
```bash
dquilt refresh
dquilt header -e
```
enter a short comment about the patch.

Repeat these steps as needed.

To play or restore the patches, use series of:
```bash
dquilt pop
dquilt push
```
**NOTE**: Make sure to work after applying **all** previous patches (series of `dquilt push`).

Once your done with all patches, restore the upstream with a series of `dquilt pop`. This way the original code is kept untouched.

Also think about adapting the control file, the copyright and changes.
If your project only requires a Makefile, the default 'debian/rules' will be OK.

A nicely formated Makefile should:
- check for libraries and header locations
- provide ```all```, ```clean```, ```install``` rules.

For the current source code, a makefile would look like (after patches being applied):
```make
# This Makefile was contributed by Harry Powell (MRC-LMB)

# these are used to locate HDF and CBF headers
ifeq ($(CBFINC),)
	CBFINC=$(DESTDIR)$(prefix)/usr/include/cbflib
endif

ifeq ($(HDF5LIB),)
	HDF5LIB=$(subst -L,,$(shell pkg-config --libs-only-L hdf5)) # strip -L
endif
ifeq ($(HDF5INC),)
	HDF5INC=$(subst -I,,$(shell pkg-config --cflags hdf5)) # strip -I
endif

# simple one-shot compile
all:	
	${CC} -O3 -std=c99 -o eiger2cbf -g \
	-I${CBFINC} \
	-I${HDF5INC} \
	-Ilz4 \
	eiger2cbf.c \
	lz4/lz4.c lz4/h5zlz4.c \
	bitshuffle/bshuf_h5filter.c \
	bitshuffle/bshuf_h5plugin.c \
	bitshuffle/bitshuffle.c \
	$(strip ${HDF5LIB})/libhdf5_hl.a \
	$(strip ${HDF5LIB})/libhdf5.a \
	-lcbf -lm -lpthread -lz -ldl -lsz

clean: 
	rm -f *.o eiger2cbf

install:
	install -D eiger2cbf \
		$(DESTDIR)$(prefix)/usr/bin/eiger2cbf

distclean: clean

uninstall:
	-rm -f $(DESTDIR)$(prefix)/usr/bin/eiger2cbf

.PHONY: all install clean distclean uninstall

```

Once done, make sure you record your changes in the Debian git:
```bash
dquilt pop
git add debian
git commit . -m "added patches"
```

And save it all into Salsa, and create a Tag for the current upstream source code release:
```bash
git push
git push origin pristine-tar
git push origin upstream
git push --set-upstream git@salsa.debian.org:farhi-guest/eiger2cbf.git : --tag
```

then make sure you are still on the master branch:
```
git checkout master
```

The Debian git repo for this example is located at https://salsa.debian.org/farhi-guest/eiger2cbf

### Step 6: build the package !

Once patches work, and the debian files are OK, you may build the source package with:
```bash
gbp buildpackage -us -uc
# restore initial git repo (so that further build work)
git checkout
```
The options indicate you do not wish for this test to sign the package.

The binary packages (e.g. *_amd64.deb) and source package files (orig.tar.gz, debian.tar.xz, .dsc) 
will be created in the parent level.

### Step 7: sign the package

Check your gpg key:
- `gpg -k farhi`

Update/build targz from sources
- `origtargz`

Possibly install missing build dependencies:
`sudo mk-build-deps  -i`

Build
- `debuild`

Sign with your gpg pw at the end of the process.

Then push the package to Debian:
```
cd ..
dput <package>.changes
```

--------------------------------------------------------------------------------

## Update an existing Debian source package from an upstream

In this case we assume we already have a git repository on salsa.debian.org
For this example we work with the Debian source package:

- https://salsa.debian.org/science-team/mccode.git

It contains a `debian/watch` file:
```
version=4
opts="mode=git, repack, repacksuffix=+ds5, date=%Y%m%d%H%M%S, dversionmangle=s/\+ds\d+//, pgpmode=none" \
https://github.com/McStasMcXtrace/McCode.git HEAD
```

which indicates that the upstream code is at:

- https://github.com/McStasMcXtrace/McCode.git

### Step 1: Prepare the Debian source repository


In this steps we make sure that the `master`, `pristine-tar` and `upstream` branches exist:
```
# go into the salsa source git
cd mccode
for i in upstream pristine-tar master; do git checkout $i; git pull; done
gbp pull
```

### Step 2: Import source code from the upstream


Then we update the source code from the `watch` file:
```
gbp import-orig --uscan --pristine-tar
```

which results in the output (after a while):
```
gbp:info: Launching uscan...
Newest version of mccode on remote site is 0.0~git20240729133303.909f6ee, local version is 0.0~git20240719135548.728fd5e
       (mangled local version is 0.0~git20240719135548.728fd5e)
 => Newer package available from:
        => https://github.com/McStasMcXtrace/McCode.git HEAD
Successfully repacked ../mccode-0.0~git20240729133303.909f6ee.tar.xz as ../mccode_0.0~git20240729133303.909f6ee+ds5.orig.tar.xz, deleting 366 files from itgbp:info: Using uscan downloaded tarball ../mccode_0.0~git20240729133303.909f6ee+ds5.orig.tar.xz
What is the upstream version? [0.0~git20240729133303.909f6ee+ds5] 0.0~git20240729
gbp:info: Importing '../mccode_0.0~git20240729133303.909f6ee+ds5.orig.tar.xz' to branch 'upstream'...
gbp:info: Source package is mccode
gbp:info: Upstream version is 0.0~git20240729
gbp:info: Replacing upstream source on 'master'
gbp:info: Successfully imported version 0.0~git20240729 of ../mccode_0.0~git20240729133303.909f6ee+ds5.orig.tar.xz
```

We check that something happened. `git status` reports:
```
On branch master
Your branch is ahead of 'origin/master' by 2 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
```

### Step 3: Adapt the patches

**The legacy way**

We go through the patches. We apply the `dquilt` aliases as defined from https://www.debian.org/doc/manuals/debmake-doc/ch03.en.html
```
dquilt push
dquilt refresh # to adapt to the updated upstream
```
In case some fail, we use `dquilt push -f` and there is a need to fix the failed patch steps.
In case more patches are needed, we add/correct them:
```
dquilt new 000X-patch-name
dquilt add FILES
[change source FILES here]
dquilt refresh
dquilt header -e # add a description for that patch
```
and series of `dquilt push; dquilt pop`. At the end, make sure to revert all patches with `dquilt pop` until no patch is applied.

**The gbp way**

We shall make use of the **rebase** to merge all commits into one (so that we can push to debian in a single operation).

```
gbp pq drop
gbp pq import --time-machine=10
```

Then if some patches fail:
```
gbp pq rebase # if some patches fail
# then edit patches/files. You may use git gui
```
Once the failing patch is OK
```
gbp pq rebase --continue
```
When all is fine
```
gbp pq export # update patches
git commit . -m "comment"
```

Then in the end, update the changelog with `dch`. Check changelog formatting.

And build with `debuild -b`.
When all is fine, push to the salsa repository.

When all is perfect, push your changes to salsa:
```
for i in upstream pristine-tar master; do git checkout $i; git push; done
gbp push
```

### Update changelog and check build

First we update the local salsa repository, and get all required branches:
```
for i in upstream pristine-tar master; do git checkout $i; git pull; done
```

We start by editing the changelog with:
```
gbp dch
```
which increments version, add date and author name, as an UNRELEASED entry. 

After updating the upstream as above (`gbp import-orig --uscan --pristine-tar`), use:
```
gbp push  # sets to master
git push
```
to push back all branches to the debian git (e.g. salsa).

Make sure the build performs as expected:
```
origtargz
dgit --gbp sbuild
```
the mark the changelog ready as 'unstable' for a push to Debian:
```
dch -r
git commit . -m "New upstream version xx.yy.zz"
```
then make sure you can locally create the source package (dsc, xz):
```
dgit --gbp build-source
```
and if it fails:
```
dgit --overwrite --gbp build-source
```
then finally, push to Debian:
```
debsign ../<package>_source.changes
dput ../<package>_source.changes
```


--------------------------------------------------------------------------------

# Become Debian maintainer

This procedure is based on trust, so it may be long as you need to pass a number of certifications, which mostly will demonstrate that you know how to encrypt and decrypt messages.
This is the base of the Debian chain of trust and developper methodology.

## Step 1: Salsa (Gitlab) Account

If you do not yet have a Sala Debian Gitlab account, create one at https://salsa.debian.org/public and click on the upper right 'Register' item.

You are also strongly advised to register to some Debian mailing lists (see https://www.debian.org/MailingLists/), such as your country related community, and other topical groups.

## Step 2: Create a PGP unique key

You need a key which is the equivalent of an ID card. This is a pair of files which contain text.

Install the `gpg` package.

Then use command (see https://keyring.debian.org/creating-key.html):
```
$ gpg --gen-key --default-new-key-algo=rsa4096/cert,sign+rsa4096/encrypt
```
Enter you name and email. A key unique ID will be created, and it is stored in your `~/..gnupg ` directory.

You may further edit/see you key with the seahorse application.

You may also see your key entry with
```
$ gpg --list-sig farhi
pub   rsa4096 2024-06-24 [SC]
      74BF724473F079B71ABB867E295EBC8B5A14A5B6
uid           [ultimate] Emmanuel Farhi <emmanuel.farhi.1@gmail.com>
sig 3        295EBC8B5A14A5B6 2024-06-24  Emmanuel Farhi <emmanuel.farhi.1@gmail.com>
sig          5632906F4696E015 2024-06-29  Picca Frédéric-Emmanuel <picca@debian.org>
sub   rsa4096 2024-06-24 [E]
sig          295EBC8B5A14A5B6 2024-06-24  Emmanuel Farhi <emmanuel.farhi.1@gmail.com>
```

It is advised to send your key to the servers:
```
# to hkps://keys.openpgp.org
gpg --send-key 74BF724473F079B71ABB867E295EBC8B5A14A5B6
# to Ubuntu
gpg --keyserver keyserver.ubuntu.com --send-key 74BF724473F079B71ABB867E295EBC8B5A14A5B6
```
Change the key ID with yours.

## Step 3: New Members service

Then, you need to go to the [Debian New Members](https://nm.debian.org) (NM) web service. Then select the **Apply** item on the left panel. You will need to login. Use your salsa account.

Then you shall see the 'Requirements OK' line with red items, which are to be completed.

### 3.1 Declaration of intent

The first one is the declaration of intent. You should write a text which explains why you want to apply. Explain who you are, on which projects you have been working, and how you wish to contribute to Debian.

You will need to sign this message, but some advice will be given, as a challenge.

### 3.2 The Debian policies

You will need then to read the Debian policy documents, and certify with a signed message that you agree with all.

### 3.3 Advocate

You need to find Debian developpers that can certify that you are a trustworthy person. Your contacts will login to NM, and enter a signed message to recommend you. It is best to have two advocates. You may send a request to people you know, as well as on the public Debian mailing lists, or mentors' (see NM / Process / Contact) if you need help.

After sending your key to key-servers, you also need to have at least one Debian Developper to sign your key. Your mentor will then send you a signed message, which you need to insert in your key with:

```
# get the mentor's key
gpg --import-key your_mentor
# or all Debian keys...
sudo apt install debian-keyring
gpg --import /usr/share/keyrings/debian-keyring.gpg

# import this signature into your key (to certify it)
gpg -d msg-mentor.asc  | gpg --import

# send the updated key
gpg --send-key 74BF724473F079B71ABB867E295EBC8B5A14A5B6
gpg --keyserver keyserver.ubuntu.com --send-key 74BF724473F079B71ABB867E295EBC8B5A14A5B6
```

The role of the mentors is to check that you have some knowledge about how Debian contributions work, and that you shall participate actively in it.

### 3.4 Wait...

Check if the 'Key consistency checks` have passed.
Then you should wait for the approval of the NM 'desk'. 
Send a message to the NM desk/contact if you need assistance. 


References
---
- https://wiki.debian.org/Python/GitPackaging
- http://marquiz.github.io/git-buildpackage-rpm/gbp.import.html
- https://wiki.debian.org/DanielKahnGillmor/preferred_packaging
- https://www.debian.org/doc/manuals/debmake-doc/ch07.en.html#newversion
- https://www.debian.org/doc/manuals/debmake-doc/ch03.en.html
 

